# bitbucket-api

> Just enough bitbucket for my purposes

This is a very simple module which wrappers the

## Configuration

You need to create an App password in the bitbucket UI to use this.
This is accessible via the __User profile &rarr; Settings &rarr; App passwords__
menu.

To provide this these settings to the app, you have three options:

1. Set environment variables `BITBUCKET_API_USER` and `BITBUCKET_API_PASS`
   to the appropriate values.
2. If you use [config](https://www.npmjs.com/package/config), provide the
   config keys `bitbucket-api.auth.user` and `bitbucket-api.auth.pass`.
3. Provide an authentication object of the form `{ user: 'u', pass: 'p'}`
   to the `Repo` constructor.

These are in increasing order of precedence.

## Sample usage

The following code shows an example of the API in use

```javascript
const bitbucket = require('bitbucket-api')

const bb = new bitbucket.Repo(
  "dringtech", "bitbucket-api"
);

bb.enablePipeline();
bb.pipelineStatus
  .then((res) => console.log(JSON.stringify(res)));
```

## API Documentation

### `bitbucket.Repo`

This interacts with a repo.


`#constructor(group, repo, auth)`

Construct the Repo object to point to the group/repo namespace.
Optionally provide auth details in the form `{ user: 'u', pass: 'p' }`.

`#pipelineStatus`

Getter for the pipeline status. Returns a promise which resolves to
`true` if pipeline enabled, and `false` if pipeline disabled.

`#enablePipeline()` / `#disablePipeline()`

Method to enable the pipeline. Returns a promise which resolves with body of
the REST call.
