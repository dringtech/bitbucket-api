You can create a repository with the BitBucket REST API.

    $ curl -X POST -H "Content-Type: application/json" -d '{
        "scm": "git",
        "project": {
            "key": "Foo"
        }
    }' https://api.bitbucket.org/2.0/repositories/<username>/<repo_slug>

Push your bitbucket-pipelines.yml to your created repo.

    curl https://api.bitbucket.org/2.0/repositories/<username>/<slug>/src \
        -F /bitbucket-pipelines.yml=@bitbucket-pipelines.yml

Then enable pipeline for your project

    curl -X PUT -is -u '<username>:<password>' -H 'Content-Type: application/json' \
    https://api.bitbucket.org/2.0/repositories/<username>/<repo_slug>\pipelines_config\ \
     -d '{
            "enabled": true,
            "type": "repository_pipelines_configuration"
        }'

Finally, you can trigger a pipeline for the branch like so.

    $ curl -X POST -is -u <username>:<password> \
      -H 'Content-Type: application/json' \
     https://api.bitbucket.org/2.0/repositories/<username>/<slug>/pipelines/ \
      -d '
      {
        "target": {
          "ref_type": "branch",
          "type": "pipeline_ref_target",
          "ref_name": "<branch_name>"
        }
      }'
