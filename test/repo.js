const assert = require('assert');
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const chaiAsPromised = require('chai-as-promised');

chai.should();
chai.use(sinonChai);
chai.use(chaiAsPromised);

describe('bitbucket.Repo', function()  {
  describe('#constructor()', function()  {
    const bitbucket = require('../bitbucket');

    let method;
    beforeEach(function()  {
      method = sinon.spy(bitbucket, 'Repo');
    });

    afterEach(function()  {
      method.restore();
    });

    it('should correctly construct a Repo object', function()  {
      let repo = new bitbucket.Repo('group', 'repo', {user: 'u', pass: 'p'});
      method.should.have.been.calledOnce;
      repo.should.have.property('apiBase', 'https://api.bitbucket.org/2.0');
      repo.should.have.property('group', 'group');
      repo.should.have.property('repo', 'repo');
      repo.should.have.nested.property('requestParams.auth.user', 'u');
      repo.should.have.nested.property('requestParams.auth.pass', 'p');
    });

    // Not sure if this needs to be tested: it's a function of the config module
    it('should accept environment variables to set auth');

    // Fragile - requires that env variables are not set
    it('should throw if auth information not provided', function()  {
      (function()  {
        let repo = new bitbucket.Repo('group', 'repo');
      }).should.throw("bitbucket-api 'auth' not defined");
      method.should.have.been.calledOnce;
      method.should.have.thrown('Error');
    });
  });

  describe('#request()', function()  {
    const proxyquire = require('proxyquire');
    let bitbucket;
    let repo;
    let request;
    beforeEach(function()  {
      request = sinon.stub().resolves('Return value');
      bitbucket = proxyquire('../bitbucket', {'request-promise-native': request});
      repo = new bitbucket.Repo('group', 'repo', {user: 'u', pass: 'p'});
    });
    it('should make RESTful api calls', function()  {
      const req = repo.request({
        url: 'a',
        method: 'GET'
      })
      request.should.have.been.calledOnce;
    });
    it('should return a promise', function()  {
      const req = repo.request({
        url: 'a',
        method: 'GET'
      });
      return req.should.eventually.equal('Return value');
    })
  });

  describe('pipelineOperations', function()  {
    const bitbucket = require('../bitbucket');

    let repo = null;
    let request = null;
    beforeEach(function()  {
      repo = new bitbucket.Repo('group', 'repo', {user: 'u', pass: 'p'});
      request = sinon.stub(repo, 'request');
    });

    afterEach(function()  {
      request.restore();
    });

    describe('#pipelineStatus', function()  {
      it('should return the status of the pipeline', function()  {
        request.resolves({
          "enabled": true,
          "type": "repository_pipelines_configuration",
          "repository": {}
        });
        const res = repo.pipelineStatus;
        request.should.have.been.calledOnce;
        request.should.have.been.calledWithMatch(
          {
            method: 'GET',
            url: "https://api.bitbucket.org/2.0/repositories/group/repo/pipelines_config",
          },
        );
        return res.should.eventually.equal(true);
      });
    });
    describe('#enablePipeline()', function()  {
      it('should enable the pipeline', function()  {
        request.resolves({
          "enabled": true,
          "type": "repository_pipelines_configuration",
          "repository": {}
        });
        const res = repo.enablePipeline();
        request.should.have.been.calledOnce;
        request.should.have.been.calledWithMatch(
          {
            method: 'PUT',
            body: {
              enabled: true,
              type: "repository_pipelines_configuration",
            },
            url: "https://api.bitbucket.org/2.0/repositories/group/repo/pipelines_config",
          },
        );
        return res.should.be.fulfilled
          .and.to.eventually.have.property('enabled', true);
      });
    });
    describe('#disablePipeline()', function()  {
      it('should disable the pipeline', function()  {
        request.resolves({
          "enabled": false,
          "type": "repository_pipelines_configuration",
          "repository": {}
        });
        const res = repo.disablePipeline();
        request.should.have.been.calledOnce;
        request.should.have.been.calledWithMatch(
          {
            method: 'PUT',
            body: {
              enabled: false,
              type: "repository_pipelines_configuration",
            },
            url: "https://api.bitbucket.org/2.0/repositories/group/repo/pipelines_config",
          },
        );
        return res.should.be.fulfilled
          .and.to.eventually.have.property('enabled', false);
      });
    });
  });
});
