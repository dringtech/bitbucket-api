process.env.SUPPRESS_NO_CONFIG_WARNING = 'y';
var config = require('config');

const defaultConfigs = {
  base: 'https://api.bitbucket.org/2.0',
  auth: {
    user: process.env.BITBUCKET_API_USER,
    pass: process.env.BITBUCKET_API_PASS,
  },
};

config.util.setModuleDefaults('bitbucket-api', defaultConfigs);

module.exports = config;
