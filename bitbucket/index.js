let requestPromise = require('request-promise-native');
let config = require('./config');

class Repo {
  constructor(group, repo, auth=config.get('bitbucket-api.auth')) {
    if ( ! ( auth['user'] && auth['pass'] ) ) {
      throw new Error("bitbucket-api 'auth' not defined");
    }
    this.apiBase = config.get('bitbucket-api.base');
    this.group = group;
    this.repo = repo;
    this.requestParams = {
      auth: Object.assign({}, auth),
      json: true,
    };
  }

  request(params) {
    return requestPromise(Object.assign(params, this.requestParams));
  }

  get pipelineStatus() {
    return this.request({
      url: `${this.apiBase}/repositories/${this.group}/${this.repo}/pipelines_config`,
      method: 'GET',
    }).then((res) => {
      return res.enabled;
    });
  }

  enablePipeline() {
    return this.request({
      url: `${this.apiBase}/repositories/${this.group}/${this.repo}/pipelines_config`,
      method: 'PUT',
      body: {
        enabled: true,
        type: "repository_pipelines_configuration"
      },
    });
  }

  disablePipeline() {
    return this.request({
      url: `${this.apiBase}/repositories/${this.group}/${this.repo}/pipelines_config`,
      method: 'PUT',
      body: {
        enabled: false,
        type: "repository_pipelines_configuration"
      },
    });
  }
}

module.exports = {
  Repo: Repo,
}
